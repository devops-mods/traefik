#!/bin/bash

TRAEFIK_FILE=./config/traefik/env
JAEGER_FILE=./config/jaeger/env
METRICS_FILE=./config/grafana/env

function traefik_init() {
  if [ ! -f "$TRAEFIK_FILE" ]; then
    while true; do
      read -p "Set your domain name: " domain
      echo
      if [[ "$domain" =~ ^[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]; then
        echo "domain=$domain" >$TRAEFIK_FILE
        break
      else
        echo "Domain address $domain is invalid."
      fi
    done
    while true; do
      read -p "Set your email for acme cert: " email
      echo
      if [[ "$email" =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]; then
        echo "TRAEFIK_CERTIFICATESRESOLVERS_HTTP_ACME_EMAIL=$email" >>$TRAEFIK_FILE
        break
      else
        echo "Email address $email is invalid."
      fi
    done
    while true; do
      read -p "Set your username for traefik panel: " username
      echo
      if [[ ${#username} -ge 5 ]]; then
        break
      else
        echo "Username must contain at least 5 characters"
      fi
    done
    while true; do
      read -p "Set your password for traefik panel: " passwd
      echo
      if [[ ${#passwd} -ge 8 ]]; then
        data=$(htpasswd -nbB $username $passwd)
        ESCAPED_DATA=$(printf '%s\n' "$data" | sed -e 's/[\/&]/\\&/g')
        sed -i "s/admin:myhashpassword/$ESCAPED_DATA/g" ./config/traefik/provider/middlewares.yml
        break
      else
        echo "Password must contain at least 8 characters"
      fi
    done
    touch ./config/traefik/acme.json
    chmod 600 ./config/traefik/acme.json
  fi

}

function jaeger_init() {
  if [ ! -f "$JAEGER_FILE" ]; then
    mkdir ./config/jaeger
    echo "SPAN_STORAGE_TYPE=elasticsearch" >$JAEGER_FILE
    echo "ES_TAGS_AS_FIELDS_ALL=true" >>$JAEGER_FILE
    while true; do
      read -p "Set elasticsearch url: " EASTICSEARCH_URL
      echo
      if [[ ${#EASTICSEARCH_URL} -ge 0 ]]; then
        echo "ES_SERVER_URLS=$EASTICSEARCH_URL" >>$JAEGER_FILE
        break
      else
        echo "No elasticsearch url set"
      fi
    done
    while true; do
      read -p "Set elasticsearch username: " EASTICSEARCH_USERNAME
      echo
      if [[ ${#EASTICSEARCH_URL} -ge 0 ]]; then
        echo "ES_USERNAME=$EASTICSEARCH_USERNAME" >>$JAEGER_FILE
        break
      else
        echo "No elasticsearch username set"
      fi
    done
    while true; do
      read -p "Set elasticsearch password: " EASTICSEARCH_PASSWORD
      echo
      if [[ ${#EASTICSEARCH_URL} -ge 0 ]]; then
        echo "ES_PASSWORD=$EASTICSEARCH_PASSWORD" >>$JAEGER_FILE
        break
      else
        echo "No elasticsearch username set"
      fi
    done
  fi
}

function metrics_init() {
  if [ ! -f "$METRICS_FILE" ]; then
    echo "GF_INSTALL_PLUGINS=grafana-piechart-panel" >$METRICS_FILE
    while true; do
      read -p "Allow user to sign up (true/false): " GRAFANA_SIGNUP
      echo
      if [[ ${#GRAFANA_SIGNUP} -ge 0 ]]; then
        echo "GF_USERS_ALLOW_SIGN_UP=$GRAFANA_SIGNUP" >>$METRICS_FILE
        break
      else
        echo "No entries, set true or false"
      fi
    done
    while true; do
      read -p "Set grafana password: " GRAFANA_PASSWORD
      echo
      if [[ ${#GRAFANA_PASSWORD} -ge 8 ]]; then
        echo "GF_SECURITY_ADMIN_PASSWORD=$GRAFANA_PASSWORD" >>$METRICS_FILE
        break
      else
        echo "Password must contain at least 8 characters"
      fi
    done
  fi
}

function start() {
  export $(grep -v '^#' $TRAEFIK_FILE | xargs)
  docker-compose up -d
  if [ $? -eq 0 ]; then
    echo -e "\e[92m Stack successfully start. \n
      Url: https://traefik.$domain \n
      Url: https://jaeger.$domain \n
      Url: https://grafana.$domain \n
      Url: https://prometheus.$domain \e[39m"
  fi
}

function reset() {
  rm "${TRAEFIK_FILE}"
  rm "${JAEGER_FILE}"
  rm "${METRICS_FILE}"
  sed -i "/basicAuth/c\      basicAuth: {users: ['admin:myhashpassword']}" ./config/traefik/provider/middlewares.yml
}

function clean() {
  docker-compose down -v
  reset
}

function main() {
  clear
  echo -e "\e[92m Traefik configuration \e[39m"
  traefik_init
  sleep 2 && clear
  echo -e "\e[92m Jaeger configuration \e[39m"
  jaeger_init
  sleep 2 && clear
  echo -e "\e[92m Metrics configuration \e[39m"
  metrics_init
  sleep 2 && clear
  echo -e "\e[92m Starting services \e[39m"
  start
}
HELP="
\e[92m Use ./run.sh to start stack \n
\e[31m -c or --clean to clean config files and docker volumes and networks \n
\e[34m -r or --reset to purge config files \n
\e[39m -h or --help to display help"

if [[ "$#" -eq 0 ]]; then
  main
fi

while [[ "$#" -gt 0 ]]; do
  case $1 in
  -c | --clean)
    clean
    shift
    ;;
  -r | --reset)
    reset
    shift
    ;;
  -h | --help)
    echo -e $HELP
    exit
    ;;
  *)
    echo "Unknown parameter passed: $1"
    exit 1
    ;;
  esac
  shift
done
