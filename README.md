# Traefik

## How to install
Set the permission to execute run.sh
```console
chmod u+x run.sh
```
Execute run.sh
```console
./run.sh
```

## What do run.sh

1. Check if ./config/traefik.toml is configured
2. Check if ./config/env exist
3. If previous steps is not configure, asks the questions to set letstencrypt email, traefik domain and the username / password
4. Apply a chmod 600 to ./config/acme.json
5. Run docker-compose up -d

## You must know

### How to use run.sh

```console
 Use ./run.sh to start stack 
  -c or --clean to clean config files and docker volumes and networks 
  -r or --reset to purge config files 
  -h or --help to display help
```

### Entry points

Entry name | Port  
--- | --- |
http | 80
https | 443

### Certificates resolvers

Name: **http**

Store in ./config/acme.json on your host system and mount on /acme.json in your container

**If you delete it,  you lost all your certificates**

### Middleware

Name | Description | Values   
--- | --- | --- |
AuthUsers | BasicAuth with the login and password given at the initialisation  | - 
RateLimit | The RateLimit middleware ensures that services will receive a fair number of requests, and allows one to define what fair is. | Average: 50 <br />Burst: 25 
WhitelistAdmin | IPWhitelist accepts / refuses requests based on the client IP. | sourceRange: [127.0.0.1/32] 
Security-Headers | Add to the headers  | contentTypeNosniff: true <br /> browserXssFilter: true<br /> forceSTSHeader: true<br /> STSSeconds: 315360000<br /> STSIncludeSubdomains: true<br /> STSPreload: true<br /> AccessControlMaxAge: 100
HTTPS-only | Redirect all http request to https scheme | -
SecuredChain | Gather the https-only, security-headers and rate limit middleware | chain: {middlewares: [HTTPS-only, Security-Headers, RateLimit]}

### Logs

All logs are save to /logs into the container and a docker volume 'log' is created on the host machine 

* accessLog: /log/access.log
* log: /log/traefik.log
